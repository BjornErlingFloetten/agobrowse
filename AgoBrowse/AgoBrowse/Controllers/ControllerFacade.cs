﻿// Copyright (c) 2016-2020 Bjørn Erling Fløtten, Trondheim, Norway
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ARCAPI;
using ARCCore;

namespace AgoBrowse.Controllers {

    [Class(Description =
        "Facade for entry into the desired -" + nameof(ARCAPI.RQController) + "-.\r\n"
    )]
    [ApiController]
    public class ControllerFacade : ControllerBase {
        [HttpGet]
        [Route("{**request}")]
        [ClassMember(
            Description =
                "Catches all HTTP GET requests to this API.\r\n" +
                "\r\n" +
                "Calls -" + nameof(BaseController.CatchAll) + "- which again chooses correct controller to use in order to serve request.\r\n" +
                "\r\n" +
                "Translates generated result into a Microsoft.AspNetCore.Mvc.ContentResult instance.\r\n" +
                "\r\n" +
                "Note that you can add additional endpoints to your API, outside of this standard AgoRapide mechanism.\r\n"
        )]
        public ActionResult CatchAll(string? request) {
            if (string.IsNullOrEmpty(request)) {
                // This is not possible as it would break relative links (browser and ARCore would have different understanding of "current" location)
                // request = "RQ/";
                //
                // Instead we have to return a redirect result like this:
                return Redirect("RQ/");
            }

            return BaseController.CatchAll(strRequest: request, postData: null, dataStorage: Program.DataStorage).Use(t => new Microsoft.AspNetCore.Mvc.ContentResult {
                StatusCode = (int)t.StatusCode,
                ContentType = t.ContentType,
                Content = t.Content
            });
        }

        [HttpPost]
        [Route("{**request}")]
        [ClassMember(
            Description =
                "Catches all HTTP POST requests to this API.\r\n" +
                "\r\n" +
                "Calls -" + nameof(BaseController.CatchAll) + "- which again chooses correct controller to use in order to serve request.\r\n" +
                "\r\n" +
                "Note additional optional parameter postData (compared to catch all for HTTP GET.\r\n" +
                "\r\n" +
                "Translates generated result into a Microsoft.AspNetCore.Mvc.ContentResult instance.\r\n" +
                "\r\n" +
                "Note that you can add additional endpoints to your API, outside of this standard AgoRapide mechanism.\r\n"
        )]
        public ActionResult CatchAll(string? request, [FromForm][FromBody] string? postData) =>
             BaseController.CatchAll(strRequest: request, postData: postData, dataStorage: Program.DataStorage).Use(t => new Microsoft.AspNetCore.Mvc.ContentResult {
                 StatusCode = (int)t.StatusCode,
                 ContentType = t.ContentType,
                 Content = t.Content
             });
    }
}