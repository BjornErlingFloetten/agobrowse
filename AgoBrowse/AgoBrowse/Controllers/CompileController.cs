﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCAPI;
using ARCCore;

namespace AgoBrowse.Controllers {
    [Class(Description =
        "Compiles a hierarchical 'picture book' from the requested position in the -" + nameof(DataStorage) + "-.\r\n" +
        "\r\n" +
        "See method -" + nameof(Compile) + "-.\r\n" +
        "\r\n" +
        "Will compile from any position in the -" + nameof(DataStorage) + "- and recursively down to any sub level as long as 'URL' keys are found.\r\n" +
        "\r\n"
    )]
    public class CompileController : BaseController {

        // NOTE / TODO: Due to bug (as of May 2020) in ClassMemberAttribute as used by documentation, do not add a ClassMemberAttribute here. Document with ClassAttribute instead-
        public override ContentResult APIMethod(
            ResponseFormat format,
            int jsonDepth,
            List<IKCoded> request,
            string? postData,
            DataStorage dataStorage
        ) {
            try {
                dataStorage.Lock.EnterReadLock();
                // Note that returns HTML regardless of specified format
                return new Func<ContentResult>(() => {
                    if (!TryNavigateToLevelInDataStorage(dataStorage, request, controller: this, out var tuple, out var errorResponseHTML)) {
                        return ContentResult.CreateError(format, errorResponseHTML);
                    }
                    return Compile(
                        book: tuple.ip,
                        editUrl:
                            string.Join("/", Enumerable.Repeat("..", request.Count - 1)) +
                            (request.Count == 1 ? "" : "/") +
                            "../" +
                            /// NOTE: Note how we assume that routing Edit is used
                            "Edit/" +
                            string.Join("/", request.Select(s => s.Encoded))
                    );
                })();
            } finally {
                dataStorage.Lock.ExitReadLock();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="book"></param>
        /// <param name="editUrl">
        /// The start of the URL for editing.<br>
        /// Exmaple: If we are serving a book from "../Compile/{BookName}"<br>
        /// then the editUrl will often be a relative path like "../Edit/{BookName}<br>
        /// </param>
        /// <returns></returns>
        [ClassMember(Description =
            "Compiles a hierarchical 'picture book' from the given parameter.\r\n" +
            "\r\n" +
            "The result is a complete standalone HTML page (except for the actual images) without any external references to Javascript or CSS.\r\n" +
            "\r\n" +
            "Press spacebar on the resulting HTML page for instructions about how to browse the 'picture book'.\r\n" +
            "\r\n" +
            "The property stream format for a picture book is as follows:\r\n" +
            "\r\n" +
            "Example 1) Book without any chapters, only one level of pages.\r\n" +
            "(in the compiled HTML page Arrow Left and Right are used for browsing):\r\n" +
            "(the ... indicates the initial position in the data storage, like [userId]/[nameOfBook], for instance 'bef/MyPictureBook').\r\n" +
            ".../URL = https://somewhere.com/titlePage.jpg \r\n" +
            ".../FirstPicture/URL = https://somewhere.com/firstPicture.jpg \r\n" +
            ".../SecondPicture/URL = https://somewhere.com/secondPicture.jpg \r\n" +
            "and so on.\r\n" +
            "\r\n" +
            "Example 2) Book with subchapters.\r\n" +
            "(in the compiled HTML page Arrow Up and Down are used for moving between levels):\r\n" +
            "(the ... indicates the initial position in the data storage, like [userId]/[nameOfBook], for instance 'bef/MyAirplaneBook').\r\n" +
            ".../URL = https://somewhere.com/titlePage.jpg \r\n" +
            ".../FirstChapter/URL = https://somewhere.com/firstChapterTitlePage.jpg \r\n" +
            ".../FirstChapter/FirstPicture/URL = https://somewhere.com/firstChapterFirstPicture.jpg \r\n" +
            ".../FirstChapter/SecondPicture/URL = https://somewhere.com/firstChapterSecondPicture.jpg \r\n" +
            ".../SecondChapter/URL = https://somewhere.com/secondChapterTitlePage.jpg \r\n" +
            ".../SecondChapter/FirstSubChapter/URL = https://somewhere.com/secondChapterFirstSubChapterTitlePage.jpg \r\n" +
            ".../SecondChapter/FirstSubChapter/FirstPicture/URL = https://somewhere.com/secondChapterFirstSubChapterFirstPicture.jpg \r\n" +
            ".../SecondChapter/FirstSubChapter/SecondPicture/URL = https://somewhere.com/secondChapterFirstSubChapterFirstPicture.jpg \r\n" +
            "\r\n" +
            "NOTE: The book will be compiled with keys in alphabetical order.\r\n" +
            "\r\n" +
            "In addition to the key 'URL' the following other keys are recognized: 'Title' and 'License'.\r\n" +
            "'License' should either be an URL pointing to information about the picture and its distribution license, or a text specifying the license.\r\n" +
            "\r\n" +
            "HINT: You can use a numbering system in order to keep desired order like:\r\n" +
            "HINT: 001_Tigers/URL = https://somewhere.com/Tigers.jpg \r\n" +
            "HINT: 001_Tigers/001_SleepingTiger/URL = https://somewhere.com/SleepingTiger.jpg \r\n" +
            "HINT: 001_Tigers/002_EatingTiger/URL = https://somewhere.com/EatingTiger.jpg \r\n" +
            "HINT: 002_Lions/URL = https://somewhere.com/Lions.jpg \r\n" +
            "HINT: 002_Lions/001_SleepingLion/URL = https://somewhere.com/SleepingLion.jpg \r\n" +
            "HINT: (this ensures that tigers come before lions, and that sleeping comes before eating.)\r\n" +
            "\r\n" +
            "NOTE: In case API requests are to be done through HTTPS is is recommended to use only HTTPS picture URLs,\r\n" +
            "NOTE: due to modern browsers not accepting HTTP content within a HTTPS web page.\r\n" +
            "NOTE: This is not a problem for a locally stored HTML page however.\r\n" +
            "NOTE: (when the compiled HTML page is stored within the local file system in your computer).\r\n"
        )]
        public ContentResult Compile(IP book, string editUrl) {
            if (!book.TryGetPV<string>("URL", out var url)) {
                return ContentResult.CreateError(ResponseFormat.HTML,
                    "<p>URL at root level not found</p>\r\n" +
                    "<p>First line in the property stream format should be a picture URL for a title page, like 'URL = http://somewhere.com/picture.jpg'</p>"
                );
            }

            // We have now at least one picture. 
            // Work recursively through structure and create Javascript for browsing
            // Each of Back, Select, Previous, Next contains information about which picture to show
            var retvalBack = new System.Text.StringBuilder();
            var retvalSelect = new System.Text.StringBuilder();
            var retvalPrevious = new System.Text.StringBuilder();
            var retvalNext = new System.Text.StringBuilder();

            var knownTags = new HashSet<string> { "URL" }; // Add tags like Text and so on here (properties keys which describe pictures, that is, they are not link to other pictures)

            var keyAndUrl = new Func<string, IP, string>((key, p) => {
                var retval = "{" +
                // TODO: Decide what is correct encoding of text here. To HtmlEncode or not to HtmlEncode.
                "\"Title\":\"" + System.Web.HttpUtility.HtmlEncode(p.GetPV<string>("Title", defaultValue: "")) + "\", " + // Not guaranteed to exist, therefore default value

                /// Note: key has been encoded with <see cref="Util.CreateSafeFileOrFolderName"/> which should be save for HTML also
                "\"Key\":\"" + key + "\", " +
                "\"URL\":\"" + p.
                    GetPV<string>("URL"). // Key 'URL' is guaranteed to exist.
                    Replace("\"", "%22") + "\", " + // HACK: TODO: Find out why %22 was turned into quote here.
                "\"URL_HTMLEncoded\":\"" + System.Web.HttpUtility.HtmlEncode(p.GetPV<string>("URL")) + "\", " + // Guaranteed to exist
                "\"License\":\"" + System.Web.HttpUtility.HtmlEncode(p.GetPV<string>("License", defaultValue: "")) + "\"," + // Not guaranteed to exist, therefore default value

                /// Note: key has been encoded with <see cref="Util.CreateSafeFileOrFolderName"/> which should be safe for HTML also
                "\"Edit\":\"" + editUrl + key + "\"" +
            "};\r\n";
                return retval;
            });

            void recurser(string parentKey, IP parentP) {
                try {
                    REx.Inc();
                    var l = parentP.
                        Where(ikip => !knownTags.Contains(ikip.Key.ToString()!)). // Get only keys pointing to new pictures
                        Where(ikip => ikip.P.TryGetP<IP>("URL", out _)). // Ensure that actually contains a picture
                        OrderBy(ikip => ikip.Key.ToString()). // Always order alphabetically.
                        Select(ikip => (Key: IKCoded.FromUnencoded(ikip.Key), ikip.P)).
                        ToList();

                    if (l.Count == 0) {
                        return; // Nothing to do at this level (parent level is just a single picture)
                    }

                    // Create up back to parent level which we came from (from all pictures at this level)
                    for (var i = 0; i < l.Count; i++) {
                        retvalBack.Append("      case \"" + parentKey + "/" + l[i].Key + "\": return " + keyAndUrl(parentKey, parentP));
                    }

                    // Create down from parent level which we came from to first picture in this level
                    retvalSelect.Append("      case \"" + parentKey + "\": return " + keyAndUrl(parentKey + "/" + l[0].Key, l[0].P));
                    if (parentKey == "") {
                        // To make more intuitive, let Next be equivalent to Select at root level
                        retvalNext.Append("      case \"" + parentKey + "\": return " + keyAndUrl(parentKey + "/" + l[0].Key, l[0].P));
                    }

                    // Create previous (browse "left")
                    for (var i = 1; i < l.Count; i++) {
                        retvalPrevious.Append("      case \"" + parentKey + "/" + l[i].Key + "\": return " + keyAndUrl(parentKey + "/" + l[i - 1].Key, l[i - 1].P));
                    }

                    // Create next (browse "right")
                    for (var i = 0; i < l.Count - 1; i++) {
                        retvalNext.Append("      case \"" + parentKey + "/" + l[i].Key + "\": return " + keyAndUrl(parentKey + "/" + l[i + 1].Key, l[i + 1].P));
                    }

                    // Recurse for all levels further down
                    for (var i = 0; i < l.Count; i++) {
                        recurser(
                            parentKey: parentKey + "/" + l[i].Key,
                            parentP: l[i].P
                        );
                    }
                } finally {
                    REx.Dec();
                }
            }

            recurser("", book);

            return new ContentResult(
                statusCode: System.Net.HttpStatusCode.OK,
                contentType: "text/html",
                content:
                    "<html>\r\n" +
                    "<head>\r\n" +
                    "<meta charset=\"UTF-8\">\r\n" +
                    // From https://stackoverflow.com/questions/16485919/open-webpage-in-fullscreen-in-safari-on-ios
                    // Necessary in order for icon on Home screen on iOS device to open in full screen mode
                    "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\r\n" +
                    "<style>\r\n" + // Note: Styling (here and below) is taken from https://stackoverflow.com/questions/6169666/how-to-resize-an-image-to-fit-in-the-browser-window
                    "* {\r\n" +
                    "   margin: 0;\r\n" +
                    "   padding: 0;\r\n" +
                    "}\r\n" +
                    "</style>\r\n" +
                    "<script>\r\n" +
                    "var currentPicture = " + keyAndUrl("", book) +
                    "function back() { // Maps current picture to its 'go back' picture (up one hierarhical level). Corresponds to ArrowUp / Escape\r\n" +
                    "   switch (currentPicture.Key) {\r\n" +
                    retvalBack.ToString() + "\r\n" +
                    "      default: return undefined;\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function select() { // Maps current picture to its 'select' picture (down one hierarhical level / select a topic, first picture in that topic). Corresponds to ArrowDown / Enter\r\n" +
                    "   switch (currentPicture.Key) {\r\n" +
                    retvalSelect.ToString() + "\r\n" +
                    "      default: return undefined;\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function previous() { // Maps current picture to its 'previous' picture. Corresponds to ArrowLeft\r\n" +
                    "   switch (currentPicture.Key) {\r\n" +
                    retvalPrevious.ToString() + "\r\n" +
                    "      default: return undefined;\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function next() { // Maps current picture to the 'next' picture. Ordinary browsing. Corresponds to ArrowRight\r\n" +
                    "   switch (currentPicture.Key) {\r\n" +
                    retvalNext.ToString() + "\r\n" +
                    "      default: return undefined;\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function toggleBetweenShowingInstructionsAndShowingPicture() {\r\n" +
                    "   t = document.getElementById(\"text\");\r\n" +
                    "   p = document.getElementById(\"container\");\r\n" +
                    "   if (t.style.display === \"none\") {\r\n" +
                    "     t.style.display = \"block\";\r\n" +
                    "     p.style.display = \"none\";\r\n" +
                    "   } else {\r\n" +
                    "     t.style.display = \"none\";\r\n" +
                    "     p.style.display = \"inline\";\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function getNewURL(key) {\r\n" +
                    "   switch(key) {\r\n" +
                    "      case \"Escape\":\r\n" +
                    "      case \"ArrowUp\": return back();\r\n" +
                    "      case \"ArrowDown\":\r\n" +
                    "      case \"Enter\": return select();\r\n" +
                    "      case \"ArrowLeft\": return previous();\r\n" +
                    "      case \"ArrowRight\": return next();\r\n" +
                    "      default: return undefined;\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function convertMousePositionToPotentialKeypress(e) {\r\n" +
                    "   var pic = document.getElementById(\"picture\");\r\n" +
                    "   // Note that simply asking for pic.left or pic.top returns undefined.\r\n" +
                    "   var picLeft = (document.body.clientWidth - pic.width) / 2;\r\n" +
                    "   var picTop = (document.body.clientHeight - pic.height) / 2;\r\n" +
                    "  // Assume that picture is centered in browser. Convert mouse click to click within picture (use 0 if outside)\r\n" +
                    "   var clientX = e.clientX - picLeft;\r\n" +
                    "   var clientY = e.clientY - picTop;\r\n" +
                    "   if ((clientX < (pic.width / 5)) && (clientY < (pic.height / 5))) { // Top left corner is reserved for help\r\n" +
                    "      return(\" \");\r\n" +
                    "   } else if (clientX < (pic.width / 5)) { // Left one-fifth is reserved for left (previous picture)\r\n" +
                    "      return(\"ArrowLeft\");\r\n" +
                    "   } else if (clientY < (pic.height / 5)) { // Top one-fifth is reserved for up (back / up one hierarchical level)\r\n" +
                    "      return(\"ArrowUp\");\r\n" +
                    "   } else if (clientY > (pic.height - (pic.height / 5))) { // Bottom one-fifth is reserved for down (select topic)\r\n" +
                    "      return(\"ArrowDown\");\r\n" +
                    "   } else { // All other positions means show next picture (ordinary browsing)\r\n" +
                    "      return(\"ArrowRight\");\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "var lastMouseMoveEvent = undefined;\r\n" +
                    "var ignoreSetCursor = false; // TRUE when picture is loading (showing 'progress' / wait as cursor)\r\n" +
                    "function setCursor(e) { // Set appearance of cursor\r\n" +
                    "   if (ignoreSetCursor) return;\r\n" +
                    "   if (e == undefined) e = lastMouseMoveEvent;\r\n" +
                    "   if (e == undefined) return; // Give up\r\n" +
                    "   var k = convertMousePositionToPotentialKeypress(e);\r\n" +
                    "   if (k == \" \") {\r\n" +
                    "       document.body.style.cursor = \"help\";\r\n" +
                    "   } else if (getNewURL(k) != undefined) {\r\n" +
                    "       document.body.style.cursor = \"pointer\";\r\n" +
                    "   } else {" +
                    "       document.body.style.cursor = \"auto\";\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function processKeypress(key) {\r\n" +
                    "   switch(key) {\r\n" +
                    "      case \" \": // Space toggles between showing picture and showing instructions\r\n" +
                    "          toggleBetweenShowingInstructionsAndShowingPicture();\r\n" +
                    "          return;\r\n" +
                    "   }\r\n" +
                    "\r\n" +
                    "   temp = getNewURL(key);\r\n" +
                    "   if (temp == undefined) {\r\n" +
                    "      // Key press was not recognized or irrelevant\r\n" +
                    "      return;\r\n" +
                    "   }\r\n" +
                    "   currentPicture = temp;\r\n" +
                    "   document.getElementById(\"currentTitle\").innerHTML = currentPicture.Title;\r\n" +
                    "   document.getElementById(\"currentKey\").innerHTML = currentPicture.Key;\r\n" +
                    "   document.getElementById(\"currentURL\").innerHTML = currentPicture.URL_HTMLEncoded;\r\n" +
                    "   document.getElementById(\"currentLicense\").innerHTML = currentPicture.License;\r\n" +
                    "   document.getElementById(\"currentEdit\").href = currentPicture.Edit;\r\n" +
                    "   clearArrows();\r\n" +
                    "   document.body.style.cursor = \"progress\";\r\n" +
                    "   ignoreSetCursor = true;\r\n" +
                    "   document.getElementById(\"picture\").src = currentPicture.URL;\r\n" +
                    "   // pictureOnLoad will now pick up the processing, after image is loaded\r\n" +
                    "   return;\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function pictureOnLoad() { // Image has been loaded\r\n" +
                    "   drawArrows();\r\n" +
                    "   ignoreSetCursor = false;\r\n" +
                    "   setCursor();\r\n" +
                    "   lastTimeout = window.setTimeout(clearArrows, 400);\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function clearArrows() {\r\n" +
                    "   var c = document.getElementById(\"canvas\");\r\n" +
                    "   var ctx = c.getContext(\"2d\");\r\n" +
                    "   ctx.clearRect(0,0,c.width,c.height);\r\n" +
                    "}" +
                    "\r\n" +
                    "function isMouse(potentialKeyPress) {\r\n" +
                    "   if (lastMouseMoveEvent == undefined) return false; // Give up\r\n" +
                    "   return convertMousePositionToPotentialKeypress(lastMouseMoveEvent) == potentialKeyPress;\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function drawArrows() { // Show arrow for each valid navigational paths \r\n" +
                    "   var c = document.getElementById(\"canvas\");\r\n" +
                    "   var p = document.getElementById(\"picture\");\r\n" +
                    "   // Avoid strange default canvas size 300 by 150\r\n" +
                    "   // https://stackoverflow.com/questions/7792788/canvas-default-size \r\n" +
                    "   c.width = p.width;\r\n" +
                    "   c.height = p.height;\r\n" +
                    "   c.style.width = p.width;\r\n" +
                    "   c.style.height = p.height;\r\n" +
                    "   var l = 40; // Length\r\n" +
                    "   var m = 10; // Distance (margin) from border\r\n" +
                    "   var s = 10; // Spacing between black and white arrow\r\n" +
                    "   var context = c.getContext(\"2d\");\r\n" +
                    "   context.font = \"30px Arial\";\r\n" +
                    "   context.fillStyle = isMouse(\" \") ? \"#fff\" : \"#bbb\";\r\n" +
                    "   context.fillText(\"?\", m, 40);\r\n" +
                    "   context.fillStyle = isMouse(\" \") ? \"#000\" : \"#444\";\r\n" +
                    "   context.fillText(\"?\", m + 20, 40);\r\n" +
                    "   if (getNewURL(\"ArrowUp\") != undefined) {\r\n" +
                    "      drawArrow(context, isMouse(\"ArrowUp\") ? \"#000\" : \"#444\", (c.width / 2) + s, l + m, (c.width / 2) + s, m);\r\n" +
                    "      drawArrow(context, isMouse(\"ArrowUp\") ? \"#fff\" : \"#bbb\", (c.width / 2) - s, l + m, (c.width / 2) - s, m);\r\n" +
                    "   }\r\n" +
                    "   if (getNewURL(\"ArrowDown\") != undefined) {\r\n" +
                    "      drawArrow(context, isMouse(\"ArrowDown\") ? \"#000\" : \"#444\", (c.width / 2) + s, c.height - l - m, (c.width / 2) + s, c.height - m);\r\n" +
                    "      drawArrow(context, isMouse(\"ArrowDown\") ? \"#fff\" : \"#bbb\", (c.width / 2) - s, c.height - l - m, (c.width / 2) - s, c.height - m);\r\n" +
                    "   }\r\n" +
                    "   if (getNewURL(\"ArrowLeft\") != undefined) {\r\n" +
                    "      drawArrow(context, isMouse(\"ArrowLeft\") ? \"#000\" : \"#444\", l + m, (c.height / 2) + s, m, (c.height / 2) + s);\r\n" +
                    "      drawArrow(context, isMouse(\"ArrowLeft\") ? \"#fff\" : \"#bbb\", l + m, (c.height / 2) - s, m, (c.height / 2) - s);\r\n" +
                    "   }\r\n" +
                    "   if (getNewURL(\"ArrowRight\") != undefined) {\r\n" +
                    "       drawArrow(context, isMouse(\"ArrowRight\") ? \"#000\" : \"#444\",  c.width - l - m, (c.height / 2) + s, c.width - m, (c.height / 2) + s);\r\n" +
                    "       drawArrow(context, isMouse(\"ArrowRight\") ? \"#fff\" : \"#bbb\", c.width - l - m, (c.height / 2) - s, c.width - m, (c.height / 2) - s);\r\n" +
                    "   }\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "function drawArrow(context, strokeStyle, fromx, fromy, tox, toy) { // Borrowed from https://stackoverflow.com/questions/808826/draw-arrow-on-canvas-tag \r\n" +
                    "   var l = 10; // length of head in pixels\r\n" +
                    "   var dx = tox - fromx;\r\n" +
                    "   var dy = toy - fromy;\r\n" +
                    "   var a = Math.atan2(dy, dx);\r\n" +
                    "   context.beginPath();\r\n" +
                    "   context.strokeStyle = strokeStyle;\r\n" +
                    "   context.lineWidth = 4;\r\n" +
                    "   context.moveTo(fromx, fromy);\r\n" +
                    "   context.lineTo(tox, toy);\r\n" +
                    "   context.moveTo(tox, toy); // Necessary for lineWidth > 1\r\n" +
                    "   context.lineTo(tox - l * Math.cos(a - Math.PI / 6), toy - l * Math.sin(a - Math.PI / 6));\r\n" +
                    "   context.moveTo(tox, toy);\r\n" +
                    "   context.lineTo(tox - l * Math.cos(a + Math.PI / 6), toy - l * Math.sin(a + Math.PI / 6));\r\n" +
                    "   context.stroke();\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "window.onload = function() { // Show hint for how to use mouse click\r\n" +
                    // Attemp to do requestFullscreen will result in
                    // 2451 Failed to execute 'requestFullscreen' on 'Element': API can only be initiated by a user gesture
                    // "   document.body.requestFullscreen();\r\n" +
                    "   drawArrows();\r\n" +
                    "   // Do not delete (time out) arrows at initial load.\r\n" +
                    "   // lastTimeout = window.setTimeout(clearArrows, 2000);\r\n" +
                    "}\r\n" +
                    "\r\n" +
                    "document.onkeydown = function(e) { // Process key press\r\n" +
                    "   processKeypress(e.key); // All other keypresses are supposed to be navigation\r\n" +
                    "};\r\n" +
                    "\r\n" +
                    "var lastTimeout = undefined;\r\n" +
                    "document.onmousemove = function(e) { // Show hint for how to use mouse click\r\n" +
                    "   drawArrows();\r\n" +
                    "   lastMouseMoveEvent = e;" +
                    "   setCursor(e);\r\n" +
                    "   if (lastTimeout != undefined) window.clearTimeout(lastTimeout);\r\n" +
                    "   lastTimeout = window.setTimeout(clearArrows, 2000);\r\n" +
                    "};\r\n" +
                    "\r\n" +
                    "document.onclick = function(e) { // Process mouse click\r\n" +
                    "   processKeypress(convertMousePositionToPotentialKeypress(e));\r\n" +
                    "};\r\n" +
                    "\r\n" +
                    "var firstTouch = undefined;\r\n" +
                    "document.ontouchstart = function(e) { // Process swipes / flicks\r\n" +
                    "   if (e.touches.length != 1) return; // Too complicated\r\n" +
                    "   firstTouch = e.touches[0];\r\n" +
                    "   drawArrows();\r\n" +
                    "   if (lastTimeout != undefined) window.clearTimeout(lastTimeout);\r\n" +
                    "   lastTimeout = window.setTimeout(clearArrows, 2000);\r\n" +
                    "};\r\n" +
                    "\r\n" +
                    "var lastTouch = undefined;\r\n" +
                    "document.ontouchmove = function(e) { // Process swipes / flicks\r\n" +
                    "   if (e.touches.length != 1) {\r\n" +
                    "      // Too complicated\r\n" +
                    "      firstTouch = undefined;\r\n" +
                    "      return;\r\n" +
                    "   }\r\n" +
                    "   lastTouch = e.touches[0];\r\n" +
                    "};\r\n" +
                    "\r\n" +
                    "document.ontouchend = function(e) { // Process swipes / flicks\r\n" +
                    "   // Note: Parameter e is of no value now\r\n" +
                    "   if (firstTouch == undefined) return; // Give up\r\n" +
                    "   if (lastTouch == undefined) return; // Give up\r\n" +
                    "   var diffX = lastTouch.clientX - firstTouch.clientX;\r\n" +
                    "   var diffY = lastTouch.clientY - firstTouch.clientY;\r\n" +
                    "   if (Math.abs(diffX) > Math.abs(diffY)) { // Treat as left/right swipe\r\n" +
                    "       if (Math.abs(diffX) < 100) return; // Insufficient length of swipe\r\n" +
                    "       if (diffX > 0) {\r\n" +
                    "           processKeypress(\"ArrowLeft\");\r\n" +
                    "       } else {\r\n" +
                    "           processKeypress(\"ArrowRight\");\r\n" +
                    "       }\r\n" +
                    "   } else { // Treat as up/down swipe\r\n" +
                    "       if (Math.abs(diffY) < 100) return; // Insufficient length of swipe\r\n" +
                    "       if (diffY > 0) {\r\n" +
                    "           processKeypress(\"ArrowUp\");\r\n" +
                    "       } else {\r\n" +
                    "           processKeypress(\"ArrowDown\");\r\n" +
                    "       }\r\n" +
                    "   }\r\n" +
                    "};\r\n" +
                    "\r\n" +
                    "</script>\r\n" +
                    "</head>\r\n" +
                    "<body style=\"background-color:black;\">\r\n" +
                    "<div id=\"imgbox\" style=\"display:grid; height:100vh;\">\r\n" +
                    "\r\n" +
                    "   <!-- The text part with information -->\r\n" +
                    "   <div id=\"text\" style=\"display:none; background-color:white;\">\r\n" +
                    "      <p style=\"margin:10px\">\r\n" +
                    "      <u>Back to picture</u>\r\n" +
                    "      </p>\r\n" +
                    "      <p style=\"margin:10px\">\r\n" +
                    "      Instructions:<br>\r\n" +
                    "      Browse with Left and Right arrow. \r\n" +
                    "      Enter or Down arrow for selecting sub-level. \r\n" +
                    "      Escape or Up arrow for back to super-level.<br>\r\n" +
                    "      Space for toggling between showing these instructions and showing picture.<br>\r\n" +
                    "      TIP: For small pictures, use Zoom functionality in browser (CTRL + / CTRL - in Chrome on a PC). \r\n" +
                    "      (Zoom on a PC will not affect big pictures, but you can use pinch gestures)<br>\r\n" +
                    "      TIP: Use full screen view (F11 in Chrome on a PC)<br>\r\n" +
                    "      <br>\r\n" +
                    "      </p>\r\n" +
                    "      <p style=\"margin:10px\">Title: <b><label id=\"currentTitle\">" + "" + "</label></b></p>\r\n" +
                    "      <p style=\"margin:10px\">Key: <label id=\"currentKey\">" + "" + "</label></p>\r\n" +
                    "      <p style=\"margin:10px\">URL: <label id=\"currentURL\">" + book.GetPV<string>("URL") + "</label></p>\r\n" +
                    "      <p style=\"margin:10px\">License: <label id=\"currentLicense\">" + book.GetPV<string>("License", "") + "</label></p>\r\n" +
                    "      <p style =\"margin:10px\">\r\n" +
                    "      <a id=\"currentEdit\" href=\"" + editUrl + "\">Edit</a>" +
                    "      </p>\r\n" +
                    "   </div>\r\n" + // text
                    "\r\n" +
                    "   <!-- The picture part with actual picture -->\r\n" +
                    "   <div id=\"container\" style=\"position:relative; display:inline; margin:auto;\"" +
                    "   >\r\n" +
                    "      <img id=\"picture\" style=\"user-select: none; max-width: 100vw; max-height: 100vh; z-index:1\" src=\"" + url + "\" onload=\"pictureOnLoad();\"/>\r\n" +
                    "\r\n" +
                    "      <canvas id=\"canvas\" style=\"max-width:100vw; max-height:100vh; top:0; left:0; position:absolute; z-index:1;\"/>\r\n" +
                    "    </div>\r\n" + // Container
                    "</div>\r\n" + // imgbox
                    "</body></html>"
            );
        }
    }
}