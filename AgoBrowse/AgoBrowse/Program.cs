// Copyright (c) 2016-2020 Bj�rn Erling Fl�tten, Trondheim, Norway

using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ARCCore;
using ARCDoc;
using ARCAPI;

namespace AgoBrowse {

    public class Program {

        public static readonly IK NodeId = IKString.FromString(System.Environment.MachineName);
        public static DataStorage DataStorage { get; private set; } = null!; // Initialized here only in order to get rid of compilation warning. 

        public static void Main(string[] args) {
            System.Threading.Thread.CurrentThread.Name = "MainThread";

            /// Include all assemblies in which your controllers and <see cref="AgoRapide.BaseEntity"/>-derived classes resides.
            UtilCore.Assemblies = new List<System.Reflection.Assembly> {
                typeof(ARCCore.ClassAttribute).Assembly,
                typeof(ARCDoc.Documentator).Assembly,
                typeof(ARCQuery.QueryExpression).Assembly,
                typeof(ARCAPI.BaseController).Assembly,
                typeof(AgoBrowse.Program).Assembly
                };

            using var streamProcessor = StreamProcessor.CreateBareBonesInstance(NodeId);
            DataStorage = DataStorage.Create(NodeId, storage: new Root(), streamProcessor);
            InitStreamProcessor(streamProcessor);
            AddController.AllowFilter = new List<Subscription> {
                Subscription.Parse("-" + nameof(PSPrefix.dt)),
                Subscription.Parse("+" + nameof(PSPrefix.dt) + "/*"),
                Subscription.Parse("-" + nameof(PSPrefix.dt) + "/bef/*") // bef is the demo book, do not make changes to it.
            };
            Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => webBuilder.UseStartup<Startup>()).Build().Run();
        }


        [ClassMember(Description =
            "Initializes stream processor.\r\n" +
            "Must be called after -" + nameof(DataStorage) + "- has been initialized."
        )]
        public static void InitStreamProcessor(StreamProcessor streamProcessor) {

            // TODO: Consider using subscription here instead.
            // /// <see cref="Subscription"/> is used here for pedagogical reasons. It is more efficient to compare direct against 'dt/'.
            // var subscription = Subscription.Parse("+" + nameof(PropertyStreamLinePrefix.dt) + "/");
            var dt = nameof(PSPrefix.dt) + "/";

            streamProcessor.OutsideLocalReceiver = s => {
                // Initial version of OutsideLocalReceiver, only used at application startup

                // Note absence of locking here (because single threaded) (we could as well have used locking, it really makes no difference as there will be no contention anyway)
                if (!s.StartsWith(dt)) return;
                // Note more strict handling at application startup. If fails then application will not start.
                PropertyStreamLine.ParseAndStore(DataStorage.Storage, s);
            };

            // This call may take some time.
            streamProcessor.Initialize();

            streamProcessor.OutsideLocalReceiver = s => {
                // 'Permanent' (within application lifetime) version of OutsideLocalReceiver
                if (!s.StartsWith(dt)) return;

                try {
                    DataStorage.Lock.EnterWriteLock(); // Use locking because this is a multi threaded context now.
                    // Note less strict handling when application is running.
                    PropertyStreamLine.ParseAndStoreFailSafe(DataStorage.Storage, s);
                } finally {
                    DataStorage.Lock.ExitWriteLock();
                }
            };
        }
    }
}