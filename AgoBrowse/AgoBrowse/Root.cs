﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ARCCore;

namespace AgoBrowse {
    [Class(Description = 
        "Contains the user friendly welcome text (front page) for the application.\r\n" +
        "\r\n" +
        "An instance of this class is used as root-element in -" + nameof(ARCAPI.DataStorage.Storage) + "-.\r\n" +
        "Used in order for -" + nameof(ToHTMLSimpleSingle) + "- to override the default rather useless listing of API internal information."
    )]
    public class Root : PRich {

        [ClassMember(Description =
            "Alternative ('override') to -" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "-.\r\n" +
            "\r\n" +    
            "Contains hand coded HTML introducing AgoBrowse.\r\n" +
            "\r\n" +
            "NOTE: The mere existence of this method is not sufficient in order to avoid " +
            "-" + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- being called.\r\n" +
            "(extension method will be chosen anyway if instance is of type -" + nameof(IP) + "-.\r\n" +
            "See " + nameof(ARCDoc.Extensions.ToHTMLSimpleSingle) + "- for more details about this issue.\r\n"
        )]
        public string ToHTMLSimpleSingle(bool prepareForLinkInsertion = false, List<IKCoded>? linkContext = null) =>
            "<p>AgoBrowse: Picture books.</p>" +
            "<h1>The pictures</h1>\r\n" +
            "<p>" +
            /// TODO: Consider letting <see cref="Controllers.CompileController"/> extract sub-chapters automatically instead
            "Animals: " +
            "<a href=\"../Compile/dt/bef/Dinosaurs\">Dinosaurs</a>. " +
            "<a href=\"../Compile/dt/bef/Dyr\">Animals</a>. " +
            "<br>\r\n" +
            "<br>\r\n" +
            "Aircraft related: " +
            "<a href=\"../Compile/dt/bef/Fly/Gamlefly\">Old aircraft</a>. " +
            "<a href=\"../Compile/dt/bef/Fly/Hangarskip\">Aircraft carriers</a>. " +
            "<a href=\"../Compile/dt/bef/Fly/RareFly\">Strange aircraft</a>. " +
            "<a href=\"../Compile/dt/bef/Fly/Småfly\">Small aircraft</a>. " +
            "<a href=\"../Compile/dt/bef/Fly/Storefly\">Big aircraft</a>. " +
            "<a href=\"../Compile/dt/bef/Fly/WWI\">Aircraft from World War I</a>. " +
            "<a href=\"../Compile/dt/bef/Fly/WWII\">Aircraft from World War II</a>. " +
            "<br>\r\n" +
            "<br>\r\n" +
            "Miscellaneous: " +
            "<a href=\"../Compile/dt/bef/SpaceSolarSystem\">The solar system</a>. " +
            "<a href=\"../Compile/dt/bef/HistoryHuman\">Human history</a>. " +
            "<a href=\"../Compile/dt/bef/HistoryCar\">History of cars</a>. " +
            "<a href=\"../Compile/dt/bef/NorskeFugler\">Birds of Norway</a>. " +
            "<br>\r\n" +
            "<br>\r\n" +
            "Stuff for toddlers: " +
            "<a href=\"../Compile/dt/bef/Toddler/Hus\">Household objects</a>. " +
            "<a href=\"../Compile/dt/bef/Toddler/Dyr\">Animals</a>. " +
            "<a href=\"../Compile/dt/bef/Toddler/Season\">Seasons</a>. " +
            "<a href=\"../Compile/dt/bef/Toddler/Icon\">Icons</a>. " +
            "<a href=\"../Compile/dt/bef/Toddler/Telle\">Counting</a>. " +
            "<br>\r\n" +
            "<br>\r\n" +
            "</p>\r\n" +
            "<h1>Instructions</h1>\r\n" +
            "<p>\r\n" +
            "If you are not afraid of hierarchical ordering and corresponding more complex navigation,<br>" +
            "you may try <a href=\"../Compile/dt/bef\">all chapters mentioned above</a> in one go. " +
            "<br>\r\n" +
            "<br>\r\n" +
            "When viewing pictures, press SPACE in order to view instructions.\r\n" +
            "</p>\r\n" +
            "<h1>Create your own books</h1>\r\n" +
            "<p>\r\n" +
            "You are also welcome to create you own books here.<br>\r\n" +
            "<br>\r\n" +
            "At the time of writing (April 2021), there really is no specific editing functionality tailored against picture books.<br>\r\n" +
            "This means that some skills with the keyboard and abstract thinking is necessary.<br>\r\n" +
            "The editing process consists of directly issuing so called API calls from your browser, of which there are four, Add, RQ (REST Query), Edit and Compile.<br>\r\n" +
            "They all operate against the hierarchy of your book which is like /{yourUserId}/{bookName}/{chapter}/{subChapter}<br>\r\n" +
            "<br>\r\n" +
            "Step 1, Create your own user-id by<br>\r\n" +
            "Add/dt/{yourUserId}, <a href=\"http://vifero.no/Add/dt/JohnSmith\">Example</a><br>\r\n" +
            "<br>\r\n" +
            "Step 2, your own book (or really chapter) by<br>\r\n" +
            "Add/dt/{yourUserId}/{bookName}.<br>\r\n" +
            "<br>\r\n" +
            "Step 3, Add your title picture like<br>\r\n" +
            "Add/dt/{yourUserId}/{bookName}/URL = https://somewhere.com/somepictureusefulastitlepicture.jpg <br>\r\n" +
            "(you may actually leave out step 1 and 2 above, and go straight to step 3.)\r\n" +
            "<br>\r\n" +
            "Add more pictures like<br>\r\n" +
            "Add/dt/{yourUserId}/{bookName}/{someKey}/URL = https://somewhere.com/somepicture.jpg <br>\r\n" +
            "Add/dt/{yourUserId}/{bookName}/{someOtherKey}/URL = https://somewhere.com/someotherpicture.jpg <br>\r\n" +
            "<br>\r\n" +
            "Use<br>\r\n" +
            "Compile/dt/{yourUserId}/{bookName}<br>\r\n" +
            "in order to view your book, and <br>\r\n" +
            "Edit/dt/{yourUserId}/{bookName}<br>\r\n" +
            "in order to edit it.<br>\r\n" +
            "You may also edit single pictures like<br>\r\n" +
            "Edit/dt/{yourUserId}/{bookName}/{someKey}<br>\r\n" +
            "You can view the keys / datastructure for all your books by<br>\r\n" +
            "RQ/dt/{yourUserId} <a href=\"http://vifero.no/RQ/dt/bef\">Example</a><br>\r\n" +
            "<br>\r\n" +
            "Click the 'Edit' link from some of the books above in order to learn syntax.<br>\r\n" +
            "<br>\r\n" +
            "Tip: You may use your own favourite text-editor in order to construct the 'property stream' and paste it back into the Edit dialog when complete.<br>\r\n" +
            "<br>\r\n" +
            "Recognized keys are 'URL' (necessary), 'Title' and 'License'.<br>\r\n" +
            "TIP: Wikipedia is a good source for licensed material. SHIFT-click or CTRL-click on a picture in order to get all details.<br>\r\n" +
            "Use key 'Invalid' (without any '= {value}') in order to delete pictures.<br>\r\n" +
            "Example: Add/dt/{yourUserId}/{bookName}/{someKey}/Invalid<br>\r\n" +
            "<br>\r\n" +
            "Sorry for it being a bit cumbersome to start with. Maybe I will make it more user friendly in the future. " +
            "On the other hand, once you get used to it, it actually works quite fast.<br>\r\n" +
            "<br>\r\n" +
            "<br>\r\n" +
            "You can edit at any level in the hierarchy.<br>\r\n" +
            "All the following API calls are equivalent:<br>\r\n" +
            "Add/John/Tigers/URL = http://somewhere.com/pictureOfTiger.jpg <br>\r\n" +
            "Edit/John and writing 'Tigers/URL = http://somewhere.com/pictureOfTiger.jpg' <br>\r\n" +
            "Edit/John/Tigers and writing 'URL = http://somewhere.com/pictureOfTiger.jpg' <br>\r\n" +
            "Edit/John/Tigers/URL and writing ' = http://somewhere.com/pictureOfTiger.jpg' <br>\r\n" +
            "<br>\r\n" +
            "</p>\r\n" +
            "<h1>Miscellaneous links</h1>\r\n" +
            string.Join("", new List<(string url, string text)> {
                (nameof(PSPrefix.doc),"AgoRapide (ARCore) documentation"),
                (nameof(PSPrefix.app),"Internal application state")
            }.Select(tuple =>
                "<p><a href=\"" + System.Net.WebUtility.UrlEncode(tuple.url) + "\">" +
                System.Net.WebUtility.HtmlEncode(tuple.text) + "</a></p>"
            )) +
            "\r\n" +
            (
                /// Information from <see cref="PropertyStreamLine.StoreFailure"/>
                /// TODO: Link this better together, create separate class called "ParseOrStoreFailure"
                !Program.DataStorage.Storage.TryGetP<IP>("ParseOrStoreFailure", out var f) ? "" : (
                    "<p style=\"color:red\">ERROR: Some -" + nameof(ARConcepts.PropertyStream) + "- content did not parse correctly. Details: <a href=\"ParseOrStoreFailure\">ParseOrStoreFailure</a></p>\r\n"
            )) +
            "\r\n" +
            "<h1>About AgoBrowse</h1>\r\n" +
            "<p>Note: <a href=\"https://bitbucket.org/BjornErlingFloetten/agobrowse\">Source code at Bitbucket</a>. " +
            "See also comments on <a href=\"https://news.ycombinator.com/item?id=26697285\">Hacker News</a>." +
            "</p>\r\n" +
            "<p>\r\n" +
            "Note: AgoBrowse has been written for my two smallest kids. " +
            "There is an emphasis on seeing high-resolution pictures in full screen, without any distractions. " +
            "With a little kid on my lap it is just too difficult concentrating on pictures through a standard web search. " +
            "The pictures would be low resolution or cumbersome to show in full screen. " +
            "And the kid quickly gets confused by all the other distracting elements on a web page. " +
            "<br>\r\n" +
            "<br>\r\n" +
            "In AgoBrowse the only allowed user interaction is click or flick left / right. " +
            "I prefer the keyboard for navigating myself, but have also added support for mouse, and for flicking on a \"smart\"-phone.\r\n" +
            "<br>\r\n" +
            "<br>\r\n" +
            "I have been longing for such an application for many years, and finally, thanks to Covid-19 (!), I found the time for this. " +
            "<br>\r\n" +
            "<br>\r\n" +
            "In addition to satisfying my own itch, " +
            "AgoBrowse is also a demonstration of <a href=\"https://bitbucket.org/BjornErlingFloetten/arcore\">AgoRapide</a>, " +
            "my open source library for building data-oriented backend applications using .NET Standard 2.1. " +
            "<br>\r\n" +
            "<br>\r\n" +
            "The corresponding HTML page (with Javascript and CSS) is quite minimal, consisting of only around 300 lines of code (apart from the picture URLs of course). " +
            "There is <i>no use of any library of any kind</i>. \r\n" +
            "It is therefore lightning fast and very suitable for low-performance devices. " +
            "Note that you can even save the generated HTML page locally / add it to our home screen. " +
            "As long as the pictures are cached you can even browse locally without an Internet connection.\r\n" +
            "</p>\r\n" +
            "<p>Author: Bjørn Erling Fløtten, Trondheim, Norway. bef_agobrowse@bef.no</p>\r\n" +
            "<h1>Other recommended websites</h1>\r\n" +
            "<p>Lightweight fast loading web sites offering development guidance, in increasing order of complexity:<br>\r\n" +
            "<a href=\"https://motherfuckingwebsite.com\">Original</a> " +
            // "<a href=\"https://bettermotherfuckingwebsite.com\">Better</a> " + // Does not load as of Jan 2021
            "<a href=\"https://perfectmotherfuckingwebsite.com\">Perfect</a> " +
            "<a href=\"https://thebestmotherfucking.website\">Best</a> " +
            "<a href=\"https://thebestmotherfuckingwebsite.co\">Also best</a>" +
            "... you get the picture :)<br>" +
            "</p>";
    }
}
