REM Ensure that Build | Publish AgoBrowse was done in Visual Studio.
REM
REM Ensure that station Z: is mapped to root of the web server
REM
REM In other words, ensure that the following folders are available:
REM Z:\etc\apache2\sites-enabled (for copying of file vifero.conf)
REM Z:\etc\systemd\system (for copying of file vifero.service)
REM Z:\var\www\app\vifero (for copying of vifero\bin\Release\net5.0\publish)

PAUSE

COPY vifero.conf Z:\etc\apache2\sites-enabled
COPY vifero.service Z:\etc\systemd\system
COPY ..\..\bin\Release\net5.0\publish\*.* Z:\var\www\app\vifero

REM Most relevant commands to execute on server now are:
REM   systemctl restart vifero.service

REM If vifero.conf was changed:
REM   apache2ctl configtest
REM   systemctl restart apache2

REM If vifero.service was changed:
REM   systemctl daemon-reload
REM   systemctl restart vifero.service

REM If this was initial publish, additional steps must also be taken.
REM See vifero.conf and vifero.service for details.
REM _Database.txt file in Data-folder must be copied to \var\www\app\vifero\Data
REM Also, ensure that folder \var\www\app\vifero\Data AND files within have necessary write rights.

PAUSE


