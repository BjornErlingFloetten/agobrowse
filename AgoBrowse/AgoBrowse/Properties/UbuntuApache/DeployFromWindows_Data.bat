REM Ensure that station Z: is mapped to root of the web server
REM
REM In other words, ensure that the following folders are available:
REM Z:\var\www\app\vifero\Data (for copying of vifero\Data\__Database..txt

PAUSE

COPY ..\..\Data\__Database..txt Z:\var\www\app\vifero\Data

REM Most relevant commands to execute on server now are:
REM   systemctl restart vifero.service

REM Ensure that folder \var\www\app\vifero\Data AND files within have necessary write rights.

PAUSE
