# AgoRapide 2020
AgoBrowse is a C# / .Net web-application for creating and browsing picture books. 

AgoBrowse is based on [ARCore](https://bitbucket.org/BjornErlingFloetten/ARCore) (AgoRapide 2020).

An online instance of AgoBrowse can be found [here](http://vifero.no) (Vifero.no).

For AgoRapide 2020 in general, see its Bitbucket repository [ARCore](https://bitbucket.org/BjornErlingFloetten/ARCore).

In order to compile and run AgoBrowse locally, ARCore must be cloned in a parallell folder to AgoBrowse, like git/ARCore and git/AgoBrowse.

AgoBrowse has been written for my two smallest kids. There is an emphasis on seeing high-resolution pictures in full screen, without any distractions. With a little kid on my lap it is just too difficult concentrating on pictures through a standard web search. The pictures would be low resolution or cumbersome to show in full screen. And the kid quickly gets confused by all the other distracting elements on a web page.

In AgoBrowse the only allowed user interaction is click or flick left / right. I prefer the keyboard for navigating myself, but have also added support for mouse, and for flicking on a "smart"-phone.

I have been longing for such an application for many years, and finally, thanks to Covid-19 (!), I found the time for this.

In addition to satisfying my own itch, AgoBrowse is also a demonstration of [AgoRapide](https://bitbucket.org/BjornErlingFloetten/ARCore), my open source library for building data-oriented backend applications using .NET Standard 2.1.

The corresponding HTML page (with Javascript and CSS) is quite minimal, consisting of only around 300 lines of code (apart from the picture URLs of course). There is no use of any library of any kind. It is therefore lightning fast and very suitable for low-performance devices. Note that you can even save the generated HTML page locally / add it to our home screen. As long as the pictures are cached you can even browse locally without an Internet connection.

Please feel free to contact the author at bef at bef dot no for more information.
